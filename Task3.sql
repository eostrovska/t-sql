﻿USE InternetShopDB;
go

--1. ТРИГГЕРЫ
--1) Создайте триггер, который уменьшит остатки товара на складе при добавлении товара в заказ. 

DROP TRIGGER OrderDetailsTrigger;

CREATE TRIGGER OrderDetailsTrigger
	ON OrderDetails
	FOR INSERT
AS
	Update Stocks
	SET Stocks.Qty = Stocks.Qty - Inserted.Qty
	FROM Stocks JOIN Inserted
	ON Inserted.ProductID = Stocks.ProductID
go

INSERT Orders
VALUES
(2, 3, GETDATE());
go

INSERT OrderDetails
VALUES
(27, 5, 3, 1, 200);
go

SELECT * FROM OrderDetails
SELECT * FROM Orders
SELECT * FROM Stocks
SELECT * FROM Products;
go

--2) Создайте триггер, который увеличит остатки товара на складе при удалении товара из заказа.

DROP TRIGGER OrderDetailsDELETETrigger

CREATE TRIGGER OrderDetailsDELETETrigger
	ON OrderDetails
	FOR DELETE
AS
	Update Stocks
	SET Stocks.Qty = Stocks.Qty + deleted.Qty
	FROM Stocks JOIN deleted
	ON deleted.ProductID = Stocks.ProductID
go

DELETE OrderDetails
WHERE ProductID = 3 AND OrderID = 27;
go

SELECT * FROM OrderDetails
SELECT * FROM Orders
SELECT * FROM Stocks
SELECT * FROM Products;
go

--3) Создайте триггер, который изменит остатки товара на складе при изменении количества товара в заказе.
DROP TRIGGER OrderDetailsUpdateTrigger 

CREATE TRIGGER OrderDetailsUpdateTrigger --Question: How to optimize the code
	ON OrderDetails
	FOR UPDATE
AS
	IF UPDATE(Qty)
	BEGIN
	DECLARE @difference_Qty int
	DECLARE @deleted_Qty int
	DECLARE @inserted_Qty int

		IF EXISTS
		(
			SELECT * 
			FROM inserted i
			JOIN deleted d
				ON d.ProductID = i.ProductID AND
				d.OrderID = i.OrderID
			WHERE d.Qty > i.Qty
			
		)
		BEGIN
			SET @deleted_Qty = (SELECT d.Qty 
				FROM inserted i
				JOIN deleted d
					ON d.ProductID = i.ProductID AND
					d.OrderID = i.OrderID)

			SET @inserted_Qty = (SELECT i.Qty 
				FROM inserted i
				JOIN deleted d
					ON d.ProductID = i.ProductID AND
					d.OrderID = i.OrderID)

			SET @difference_Qty = (@deleted_Qty - @inserted_Qty)
			UPDATE Stocks
			SET Qty = Qty + @difference_Qty
		END
		ELSE
		BEGIN 
			SET @deleted_Qty = (SELECT d.Qty 
				FROM inserted i
				JOIN deleted d
					ON d.ProductID = i.ProductID AND
					d.OrderID = i.OrderID)

			SET @inserted_Qty = (SELECT i.Qty 
				FROM inserted i
				JOIN deleted d
					ON d.ProductID = i.ProductID AND
					d.OrderID = i.OrderID)

			SET @difference_Qty = (@inserted_Qty - @deleted_Qty)
			UPDATE Stocks
			SET Qty = Qty - @difference_Qty
		END
	END
go

SELECT * FROM OrderDetails
SELECT * FROM Orders
SELECT * FROM Stocks
SELECT * FROM Products;
go

INSERT Orders
VALUES
(2, 3, GETDATE());
go

INSERT OrderDetails
VALUES
(35, 1, 1, 4, 150);
go

UPDATE OrderDetails
SET Qty = 3
WHERE ProductID = 8 AND OrderID = 33;
go

--4) Продемонстрируйте работу триггеров.
--   Добавьте два заказа, содержащие по три разных продукта (три продукта в одном заказе, такие же три - в другом).
--   Измените кол-во одного из продуктов в обоих заказах.
--   Удалите созданные ранее два заказа.

INSERT Orders
VALUES
(2, 3, GETDATE()), 
(2, 3, GETDATE());
go

INSERT OrderDetails
VALUES
(31, 8, 6, 1, 150), 
(31, 9, 5, 1, 150),
(31, 10, 4, 1, 150);
go

INSERT OrderDetails
VALUES
(32, 11, 6, 1, 150),
(32, 12, 5, 1, 150),
(32, 13, 4, 1, 150);
go

UPDATE OrderDetails
SET Qty = 3
WHERE ProductID = 5 AND OrderID = 31;
go

UPDATE OrderDetails
SET Qty = 3
WHERE ProductID = 5 AND OrderID = 32;
go

DELETE OrderDetails
WHERE OrderID = 31;
go

DELETE OrderDetails
WHERE OrderID = 32;
go

-- 5) Создайте триггер, который позволит удалять продукт из таблицы продуктов, только при условии, 
--    что его нет в заказах и остаток на складе нулевой.

DROP TRIGGER ProductDELETETrigger;
go

CREATE TRIGGER ProductDELETETrigger
	ON Products
	FOR DELETE
AS
	IF  NOT EXISTS
	(
		SELECT OD.ProductID FROM OrderDetails AS OD
		JOIN deleted AS D
		ON D.ID = OD.ProductID
		JOIN Stocks AS S
		ON S.ProductID = D.ID
		WHERE S.Qty <> 0
	)
	BEGIN
	--DELETE Stocks
	--WHERE Qty = 0 AND Stocks.ProductID NOT IN 
	--(SELECT ProductID FROM  OrderDetails as OD
	--JOIN Products
	--ON Products.ID = OD.ProductID)
	PRINT('GO')
	END
	ELSE
	ROLLBACK TRAN;
go
	

INSERT Products
VALUES
('Test');
go

INSERT Stocks
VALUES
(11, 0),
(12, 0),
(13, 0), 
(14, 0);
go

DELETE Stocks
WHERE Qty = 0 AND ProductID IN (11, 12, 13, 14);
go

DELETE Products
WHERE ID IN (15, 16, 17, 18);
go

--2. VIEW
--1) Создайте представление, которое показывает ID, название, цвет, описание товара.

CREATE VIEW ProductDetail   
AS SELECT Id, Name
FROM Products;
go

SELECT * FROM ProductDetail;
SELECT * FROM Products;
go

--2) Создайте представление, которое показывает ID, название, остаток товара на складе, 
--   количество проданного товара.

CREATE VIEW ProductsInfo   
AS SELECT Id, Name, S.Qty, SUM(O.Qty) AS TotalQty
FROM Products AS P
JOIN Stocks AS S
ON S.ProductID = P.ID
JOIN OrderDetails AS O
ON O.ProductID = S.ProductID
GROUP BY ID, Name, S.Qty;
go

SELECT * FROM ProductsInfo;
go

--3) Создайте представление, которое показывает ID, ФИО, должность, зарплату, премию,
--   телефон, адресс, дату рождения, семейное положение сотрдуников.

CREATE VIEW EmploeeInformation  
AS SELECT Employees.ID, FName, MName, LName, Post, Salary, PriorSalary, Phone, Address, BirthDate, MaritalStatus
FROM Employees 
JOIN EmployeesInfo 
ON Employees.ID = EmployeesInfo.ID;
go

SELECT * FROM EmploeeInformation;
go

--4) Создайте представление, которое показывает ID, ФИО, должность, общую сумму продажи
--   по сотруднику.

CREATE VIEW EmploeeSaleInformation  
AS SELECT Employees.ID, FName, MName, LName, Post, SUM(TotalPrice) AS TotalSales
FROM Employees 
JOIN Orders 
ON Employees.ID = Orders.EmployeeID
JOIN OrderDetails
ON OrderDetails.OrderID = Orders.ID
GROUP BY Employees.ID, FName, MName, LName, Post
go

SELECT * FROM EmploeeSaleInformation;
go

--5) Создайте представление, которое показывает продажи по товарам бренда Apple: 
--   название товара, проданное количество, дату продажи, город, из которого купили товар.

CREATE VIEW AppleSaleInformation  
AS SELECT Name, SUM(OrderDetails.Qty) AS TotalQty, OrderDate, City
FROM Products 
JOIN OrderDetails
ON Products.ID = OrderDetails.ProductID
JOIN Orders
ON OrderDetails.OrderID = Orders.ID
JOIN Customers
ON Customers.ID = Orders.CustomerID
WHERE Name LIKE '%Apple%'
GROUP BY Name, OrderDate, City
go

SELECT * FROM AppleSaleInformation;
go

--6) В тех случаях, когда нет необходимости сохранять в базе SQL представление, для его
--   замены можно использовать Обобщенное Табличное Выражение.
--   Вывести общую, среднюю сумму заказа, кол-во заказов и общее количество проданного 
--   товара по каждому сотруднику за последний год.

WITH emploee_order_qty_CTE(employeeID, order_qty) AS    ----Question: How to optimize the code
    (SELECT Orders.EmployeeID, COUNT(Orders.EmployeeID) AS Order_Qty
       FROM Orders
			WHERE OrderDate BETWEEN '01/01/2017' AND '12/31/2017'
			AND Orders.EmployeeID <> 0
			GROUP BY Orders.EmployeeID ), 
	employee_product_qty_CTE(employeeID, product_qty) AS
	(SELECT Orders.EmployeeID, SUM(Qty) AS ProductQty
		FROM Orders 
		JOIN  OrderDetails
		ON Orders.ID = OrderDetails.OrderID 
		   WHERE OrderDate BETWEEN '01/01/2017' AND '12/31/2017'
		   AND Orders.EmployeeID <> 0
		   GROUP BY Orders.EmployeeID),
	employee_sum_CTE(employeeID, total_sum, avg_sum) AS
	(SELECT Orders.EmployeeID, SUM(TotalPrice) AS TotalSum, AVG(TotalPrice) AS AvgSum
		FROM Orders
		JOIN OrderDetails
		ON Orders.ID = OrderDetails.OrderID
			WHERE OrderDate BETWEEN '01/01/2017' AND '12/31/2017'
			AND Orders.EmployeeID <> 0
			GROUP BY Orders.EmployeeID)
	SELECT emploee_order_qty_CTE.employeeID, order_qty, product_qty, total_sum, avg_sum
	FROM emploee_order_qty_CTE
	JOIN employee_product_qty_CTE
	ON emploee_order_qty_CTE.employeeID = employee_product_qty_CTE.employeeID
	JOIN employee_sum_CTE
	ON employee_sum_CTE.employeeID = employee_product_qty_CTE.employeeID;
	go
    
--3. PROCEDURE 
--1) Создайте процедуру, которая позволит найти покупателя по фамилии и/или городу.
--   При этом, если мы не передадим параметров, выведутся все покупатели. Также,
--   мы можем найти всех покупателей, фамилии которых начинаются, например, на Кр
--   (аналигочино и с названием города).

DROP PROC customer;
go

CREATE PROC customer
	@LastName varchar(35) = NULL,
	@City varchar(35) = NULL
AS
IF @LastName IS NOT NULL AND @City IS NOT NULL
	SELECT ID, FName, LName, City 
	FROM Customers
	WHERE LName LIKE @LastName + '%' 
	AND City LIKE @City + '%'
ELSE
IF @LastName IS NOT NULL OR @City IS NOT NULL
	SELECT ID, FName, LName, City 
	FROM Customers
	WHERE LName LIKE @LastName + '%' 
	OR City LIKE @City + '%'
ELSE				
	SELECT ID, FName, LName, City 
	FROM Customers
go

EXEC customer     
EXEC customer 'Кр' 
EXEC customer 'Прокопенко' 
EXEC customer NULL, 'Львов'
EXEC customer 'Десятова', 'Киев'
go

--2) Создайте процедуру, которая позволит найти сотрудника по фамилии и/или должности, 
--   и/или адресу. Функционал поиска должен быть таким же, как и в предыдущей процедуре.

DROP PROC employee;
go

CREATE PROC employee
	@LastName varchar(30) = NULL,
	@Post varchar(30) = NULL,
	@Address varchar(70) = NULL
AS
IF @LastName IS NOT NULL AND @Post IS NOT NULL AND @Address IS NOT NULL
	SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID
	WHERE LName LIKE @LastName + '%' 
	AND Post LIKE @Post + '%'
	AND Address LIKE @Address + '%'
ELSE
IF @LastName IS NOT NULL AND @Post IS NOT NULL OR @Address IS NOT NULL
	SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID
	WHERE LName LIKE @LastName + '%' 
	AND Post LIKE @Post + '%'
	OR Address LIKE @Address + '%'
ELSE
IF @LastName IS NOT NULL OR @Post IS NOT NULL AND @Address IS NOT NULL
	SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID
	WHERE LName LIKE @LastName + '%' 
	OR Post LIKE @Post + '%'
	AND Address LIKE @Address + '%'
ELSE
IF @LastName IS NOT NULL OR @Address IS NOT NULL AND  @Post IS NOT NULL
	SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID
	WHERE LName LIKE @LastName + '%' 
	OR Post LIKE @Post + '%'
	AND Address LIKE @Address + '%'
ELSE
IF  @Post IS NOT NULL
	SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID 
	WHERE Post LIKE @Post + '%'
ELSE				
	SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID
go

EXEC employee     
EXEC employee 'Де' 
EXEC employee 'Десятов' 
EXEC employee NULL, 'Главный директор', 'Викторкая 16/7'
EXEC employee 'Десятов', NULL, 'Викторкая 16/7'
EXEC employee 'Десятов','Главный директор',  NULL
EXEC employee NULL, 'Главный директор', NULL
EXEC employee NULL , 'Гл',  'Викторкая 16/7'
EXEC employee NULL, 'Главный директор', 'Вик'
EXEC employee NULL, NULL, 'Вик'

SELECT Employees.ID, FName, LName, Address, Post
	FROM Employees
	JOIN EmployeesInfo
	ON Employees.ID = EmployeesInfo.ID

--3) Создайте процедуру на добаление нового продукта в БД. Процедура должна принимать
--   название товара. Цвет, описание и количество принимаются опционально.
--   Добавьте с помощью процедуры три товара:
--   1 товар - Название: Product1;
--   2 товар - Название: Product2; цвет: Синий; описание: Какое-то описание; кол-во: 5;
--   3 товар - Название: Product3; кол-во: 10.
--   Для демонстрации корректного добавления сделайте выборку по товарам (название, цвет,
--   описание, остаток на складе).
DROP PROC new_product;
go

DROP PROC new_product_add;
go

CREATE PROC new_product_add
	@Name varchar(90)
AS
	INSERT Products
		VALUES
		(@Name)
	RETURN @@IDENTITY

CREATE PROC new_product        
	@Name varchar(90),
	@Colour varchar(30) = NULL,
	@Description varchar(400) = NULL, 
	@Qty int = 0
AS
IF @Colour IS NOT NULL OR @Description IS NOT NULL AND @Qty IS NOT NULL
	DECLARE @ID int

	EXEC new_product_add @Name
	SET @ID = @@IDEntity

	INSERT ProductDetails 
	(ID, Color, Description)
	VALUES
	(@ID,  @Colour, @Description)

	INSERT Stocks
	(ProductID, Qty)
	VALUES
	(@ID, @Qty)
go

EXEC new_product 'Product1'
EXEC new_product 'Product2', 'Blue', 'Some Description', 3
EXEC new_product 'Product3', NULL, NULL, 3

SELECT Name, Color, Description, Qty
FROM Products
JOIN ProductDetails
ON Products.ID = ProductDetails.ID
JOIN Stocks
ON Stocks.ProductID = ProductDetails.ID



